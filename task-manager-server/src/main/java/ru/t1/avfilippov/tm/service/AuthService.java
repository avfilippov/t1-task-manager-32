package ru.t1.avfilippov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.service.IAuthService;
import ru.t1.avfilippov.tm.api.service.IPropertyService;
import ru.t1.avfilippov.tm.api.service.IUserService;
import ru.t1.avfilippov.tm.exception.field.LoginEmptyException;
import ru.t1.avfilippov.tm.exception.field.PasswordEmptyException;
import ru.t1.avfilippov.tm.exception.user.PermissionException;
import ru.t1.avfilippov.tm.model.User;
import ru.t1.avfilippov.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private final IPropertyService propertyService;

    public AuthService(final IUserService userService, final IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }


    @Override
    @NotNull
    public User check(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new PermissionException();
        @NotNull final String hash = HashUtil.salt(propertyService, password);
        if (hash==null) throw new PermissionException();
        if (!hash.equals(user.getPasswordHash())) throw new PermissionException();
        return user;
    }

}
