package ru.t1.avfilippov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.avfilippov.tm.api.service.IServiceLocator;
import ru.t1.avfilippov.tm.api.service.ITaskService;
import ru.t1.avfilippov.tm.dto.request.*;
import ru.t1.avfilippov.tm.dto.response.*;
import ru.t1.avfilippov.tm.enumerated.Sort;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.model.Task;

import java.util.List;

public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    private ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @Override
    @NotNull
    public BindTaskToProjectResponse bindTaskToProject(@NotNull final BindTaskToProjectRequest request) {
        check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String userId = request.getUserId();
        getServiceLocator().getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new BindTaskToProjectResponse();
    }

    @Override
    @NotNull
    public TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull final TaskChangeStatusByIdRequest request) {
        check(request);
        @Nullable final String id = request.getTaskId();
        @Nullable final String statusValue = request.getStatusValue();
        @Nullable final Status status = Status.toStatus(statusValue);
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().changeTaskStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @Override
    @NotNull
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull final TaskChangeStatusByIndexRequest request) {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String statusValue = request.getStatusValue();
        @Nullable final Status status = Status.toStatus(statusValue);
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().changeTaskStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @Override
    @NotNull
    public TaskClearResponse clearTask(@NotNull final TaskClearRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @Override
    @NotNull
    public TaskCompleteByIdResponse completeTaskById(@NotNull final TaskCompleteByIdRequest request) {
        check(request);
        @Nullable final String id = request.getTaskId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
        return new TaskCompleteByIdResponse(task);
    }

    @Override
    @NotNull
    public TaskCompleteByIndexResponse completeTaskByIndex(@NotNull final TaskCompleteByIndexRequest request) {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().changeTaskStatusByIndex(userId, index, Status.COMPLETED);
        return new TaskCompleteByIndexResponse(task);
    }

    @Override
    @NotNull
    public TaskCreateResponse createTask(@NotNull final TaskCreateRequest request) {
        check(request);
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @Override
    @NotNull
    public TaskListResponse listTasks(@NotNull final TaskListRequest request) {
        check(request);
        @Nullable final String sortType = request.getSort();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @Nullable final String userId = request.getUserId();
        @NotNull final List<Task> tasks = getTaskService().findAll(userId, sort);
        return new TaskListResponse(tasks);
    }

    @Override
    @NotNull
    public TaskRemoveByIdResponse removeTaskById(@NotNull final TaskRemoveByIdRequest request) {
        check(request);
        @Nullable final String id = request.getTaskId();
        @Nullable final String userId = request.getUserId();
        getTaskService().removeById(userId, id);
        return new TaskRemoveByIdResponse();
    }

    @Override
    @NotNull
    public TaskRemoveByIndexResponse removeTaskByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        getTaskService().removeByIndex(userId, index);
        return new TaskRemoveByIndexResponse();
    }

    @Override
    @NotNull
    public TaskShowByIdResponse showTaskById(@NotNull final TaskShowByIdRequest request) {
        check(request);
        @Nullable final String id = request.getTaskId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().findOneById(userId, id);
        return new TaskShowByIdResponse(task);
    }

    @Override
    @NotNull
    public TaskShowByIndexResponse showTaskByIndex(@NotNull final TaskShowByIndexRequest request) {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().findOneByIndex(userId, index);
        return new TaskShowByIndexResponse(task);
    }

    @Override
    @NotNull
    public TaskShowByProjectIdResponse showTaskByProjectId(@NotNull final TaskShowByProjectIdRequest request) {
        check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String userId = request.getUserId();
        @NotNull final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        return new TaskShowByProjectIdResponse(tasks);
    }

    @Override
    @NotNull
    public TaskStartByIdResponse startTaskById(@NotNull final TaskStartByIdRequest request) {
        check(request);
        @Nullable final String id = request.getTaskId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
        return new TaskStartByIdResponse(task);
    }

    @Override
    @NotNull
    public TaskStartByIndexResponse startTaskByIndex(@NotNull final TaskStartByIndexRequest request) {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new TaskStartByIndexResponse(task);
    }

    @Override
    @NotNull
    public UnbindTaskFromProjectResponse unbindTaskFromProject(@NotNull final UnbindTaskFromProjectRequest request) {
        check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String userId = request.getUserId();
        getServiceLocator().getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new UnbindTaskFromProjectResponse();
    }

    @Override
    @NotNull
    public TaskUpdateByIdResponse updateTaskById(@NotNull final TaskUpdateByIdRequest request) {
        check(request);
        @Nullable final String id = request.getTaskId();
        System.out.println("ENTER NAME:");
        @Nullable final String name = request.getName();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @Override
    @NotNull
    public TaskUpdateByIndexResponse updateTaskByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

}
