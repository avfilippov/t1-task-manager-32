package ru.t1.avfilippov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.request.ProjectStartByIndexRequest;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "start project by index";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-start-by-index";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull ProjectStartByIndexRequest request = new ProjectStartByIndexRequest();
        request.setIndex(index);
        getProjectEndpoint().startProjectByIndex(request);
    }

}
