package ru.t1.avfilippov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.request.TaskShowByIndexRequest;
import ru.t1.avfilippov.tm.dto.response.TaskShowByIndexResponse;
import ru.t1.avfilippov.tm.exception.entity.TaskNotFoundException;
import ru.t1.avfilippov.tm.model.Task;
import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "show task by index";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest();
        request.setIndex(index);
        @Nullable TaskShowByIndexResponse response = getTaskEndpoint().getTaskByIndex(request);
        if (response.getTask() == null) throw new TaskNotFoundException();
        showTask(response.getTask());
    }
}
